import org.junit.Assert;
import org.junit.Test;

public class TaskLetterTest {

    @Test
    public void shouldFindMinimalChangeOfLetterTest0() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit0");
        Assert.assertEquals(2, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest1() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit1");
        Assert.assertEquals(4, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest2() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit1ocen");
        Assert.assertEquals(1, taskLetter.getMinimalCounter());
    }
    @Test
    public void shouldFindMinimalChangeOfLetterTest3() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit2");
        Assert.assertEquals(16, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest4() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit2ocen");
        Assert.assertEquals(9, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest5() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit3a");
        Assert.assertEquals(44599, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest6() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit3b");
        Assert.assertEquals(480766, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest7() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit4a");
        Assert.assertEquals(1499627, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest8() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit4b");
        Assert.assertEquals(48076920, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest9() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit4ocen");
        Assert.assertEquals(6, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest10() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit5a");
        Assert.assertEquals(17536625, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest11() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit5b");
        Assert.assertEquals(1201923076, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest12() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit5ocen");
        Assert.assertEquals(899000000, taskLetter.getMinimalCounter());
    }
    @Test
    public void shouldFindMinimalChangeOfLetterTest13() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit6a");
        Assert.assertEquals(47167845, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest14() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit6b");
        Assert.assertEquals(4807692306l, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest15() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit7a");
        Assert.assertEquals(130542635, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest16() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit7b");
        Assert.assertEquals(19230769228l, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest17() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit8a");
        Assert.assertEquals(580934901, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest18() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit8b");
        Assert.assertEquals(120192307690l, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest19() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit9a");
        Assert.assertEquals(1482550867, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest20() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit9b");
        Assert.assertEquals(480769230766l, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest21() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit10a");
        Assert.assertEquals(1819136406, taskLetter.getMinimalCounter());
    }

    @Test
    public void shouldFindMinimalChangeOfLetterTest22() throws Exception {
        TaskLetter taskLetter = new TaskLetter();
        taskLetter.getMinNumberOfChange("lit10b");
                Assert.assertEquals(480769230766l, taskLetter.getMinimalCounter());
    }

}