import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

public class TaskLetter {

    private Integer n;
    private long minimalCounter;
    private char[] firstCharArray;
    private char[] secondCharArray;
    private Map<Character, Stack<Integer>> stackPermutation;
    private int[] permutationArray;
    private int[] temp;

    public void getMinNumberOfChange(String fileName) {

        readFile(fileName +".in");

        getPermutation();

        temp = new int[n];
        minimalCounter = mergeSort(0, n-1, permutationArray);

        writeFile(fileName+".out");
    }

    private void readFile(String pathname) {
        File file = new File(pathname);
        Scanner scanner = null;
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
        n = Integer.parseInt(scanner.nextLine());
        firstCharArray = scanner.nextLine().toCharArray();
        secondCharArray = scanner.nextLine().toCharArray();
    }

    private void getPermutation () {
        stackPermutation = new HashMap<Character, Stack<Integer>>();
        permutationArray = new int[n];

        for(int i = 0; i< firstCharArray.length; i++) {
            stackPermutation.put(firstCharArray[i], new Stack<Integer>());
        }

        for(int i = firstCharArray.length-1; i >=0; i--) {
            stackPermutation.get(firstCharArray[i]).push(i);
        }

        for(int i =0; i< n;i++) {
            permutationArray[i] = stackPermutation.get(secondCharArray[i]).pop();
        }
    }

    private void writeFile(String pathname) {
        PrintWriter printWriter = null;
        try {
            printWriter = new PrintWriter(pathname);
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }

        printWriter.print(minimalCounter);
        printWriter.close();
    }

    private long merge(int startIndex, int pivotIndex, int endIndex, int[] inputArray)
    {
        //Copy data input array to permutationArray array
        for (int i=startIndex; i<=endIndex; i++) {
            temp[i] = inputArray[i];
        }

        int i=startIndex;
        int j=pivotIndex+1;
        int q=startIndex;

        long counter = 0;
        long increaseCounter = 0;

        //Merge subarray and increase counter of inversion
        while (i<=pivotIndex && j<=endIndex) {
            if (temp[i]<=temp[j]) {
                inputArray[q++] = temp[i++];
                counter += increaseCounter;
            } else {
                inputArray[q++] = temp[j++];
                increaseCounter++;
            }
        }
        //Copy remaining elements and increase counter of inversion
        while (i<= pivotIndex) {
            inputArray[q++]=temp[i++];
            counter += increaseCounter;
        }

        while (j<= endIndex) {
            inputArray[q++] = temp[j++];
        }
        return counter;
    }

    private long mergeSort(int startIndex, int endIndex, int[] inputArray)
    {
        long counter = 0;
        int pivotIndex;
        //if(startIndex == endIndex) return minimalCounter;
        if (startIndex<endIndex) {
            pivotIndex=(startIndex+endIndex)/2;
            counter +=mergeSort(startIndex, pivotIndex, inputArray);
            counter +=mergeSort(pivotIndex+1, endIndex, inputArray);
            counter +=merge(startIndex, pivotIndex, endIndex, inputArray);
        }
        return counter;
    }

    public long getMinimalCounter() {
        return minimalCounter;
    }
}
